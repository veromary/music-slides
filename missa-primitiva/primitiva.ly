\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  system-system-spacing = #'((basic-distance . 2) (padding . 4))
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}

#(define (override-color-for-all-grobs color)
  (lambda (context)
   (let loop ((x all-grob-descriptions))
    (if (not (null? x))
     (let ((grob-name (caar x)))
      (ly:context-pushpop-property context grob-name 'color color)
      (loop (cdr x)))))))


stemOff = { \hide Staff.Stem }

kyrietune = \relative c'' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
  g4 a( b) b( b) g a b b \bar "||"
  g4 a( b) b( b) g a b b \bar "||"
  d4 b( c) a a b b \bar "||"
  d4 b( c) a a b b \bar "||"
  g4 a( b) b( b) g a b b \bar "||"
  g4 a( b) b( b) g a b b \bar "||"

  }


lordtune = \relative c'' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
  g4( a b2) g4 a( b) b \bar "||"
  g4( a b2) g4 a( b) b \bar "||"
  d2 b4( c) a( b) b \bar "||"
  d2 b4( c) a( b) b \bar "||"
  g4( a b2) g4 a( b) b \bar "||"
  g4( a b2) g4 a( b) b \bar "||"
  }

gloriatune = \relative c' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
e4 e g a g a g e \bar "||"
e e g( a) a \bar "'"
a g a \break \bar "" g g e \bar "||"
g a( g) e \bar "||"
g a( g) e \bar "||"
g a a( g) e \bar "||"
g a a g e \bar "||"
a g a b \break \bar "" a g a \bar "" g e \bar "||"
e g( a) \break \bar "" a a g e \bar "'"
g a a g a \break \bar "" g e \bar "||"
e g a a \bar "'"
a g a g g e \bar "||"
a a \bar "" g a b \bar "'"
a g a g e \bar "||"
e g a a a a g a b \bar "'"
a g a g e \bar "||"
e g a a \bar "" a a g a b \bar "'"
a g( a) \break \bar "" g e \bar "||"
e g a a a \break \bar "" a a a g a \bar "" b a \bar "'"
a \break \bar "" g a g e \bar "||"
e g a a \bar "" a g a g e \bar "||"
e g a \break \bar "" a g e \bar "||"
e g a a g \break \bar "" a( g) e \bar "'"
e g a( b a) \bar "||"
a a g a b a \bar "'"
a a a a \break \bar "" a a g a( b g g e) e \bar "||"
e( e f g f e) d( e) \bar "||"
} 

alleluiatune = \relative c'' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
g a b g \bar ","
a( b) a g e( d) \bar ","
g a( b) a( g) g \bar "||"
}

holytune = \relative c' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
b'4 b( a) \bar "'" 
b4 b( a) \bar "'" 
g a b~ b b a b \bar "|"
g a b b~ b a c b b a g \bar "|"
g a b b b a b \bar "|"
g a b b~ b a c b a b a \bar "" g g \bar "|"
e g( a) a a b a( g) g( a) \bar "||"
}


sanctustune = \relative c' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
b'4 b( a) \bar "'" 
b4 b( a) \bar "'" 
g a  b b b \bar "" b b b a b \bar "|"
g a b b b b b a \bar "" c b b a g \bar "|"
g a b b b a b \bar "|"
g a b b b b b a c b a \bar "" b a g \bar "|"
e g( a) a a b a( g) g( a) \bar "||"
}

weproclaimtune = \relative c'' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
a4 a g a c b a \bar ","
e \bar "" g a a b a g( a) g \bar "'"
a a f g( a) g e \bar "||"
}

whenwetune = \relative c' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
e4 g a a g a c b \bar "" a \bar ","
a a b a g( a) g g \bar "'"
a a f g( a) g e \bar "||"
}
saveustune = \relative c'' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
a4( c) a g e g a a \bar ","
a g a c( b) a b a g( a) g \bar "'"
a f g( a) g e \bar "||"
}



ourfathertune-au = \relative c'' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
g4( a) b b \bar "'"
a c b a g \bar ","
a( g) a b a a \bar"|"
b c( b) \break \bar "" a a( g) \bar ","
b a b a g g \bar ","
g( a) b a b a a \bar "|"
g a b c b a( b) a g \bar "|"
a g a( b) c b a g g \bar ","
a g a b a a g a b \break \bar "" a a \bar "|"
e g a b a g \break \bar "" b a( g) g \bar "|"
g g a a a b a( g) g \bar "||"
}


ourfathertune = \relative c'' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
g4( a) b b \bar "'"
a c b a g \bar ","
a g g( a) b a \bar"|"
b c( b) a( b) a( g) \bar ","
b a( b) a a( g) \bar ","
a b a g g( a) b a( g) a \bar "|"
b a b c b a( b) a a( g) \bar "|"
a b c( b) a b a g g \bar ","
a g a b a g g a b \break \bar "" a( b) a \bar "|"
e g a a a a \break \bar "" b a( g) g \bar "|"
g a a a a b a( g) g \bar "||"
}

lambtune = \relative c'' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
g g g( a) \bar "'"
 a a a a  g a \bar "" f g a \bar "'"
a g( a) b g a( g) \bar "||"
g g g( a) \bar "'"
 a a a a g a \bar "" f g a \bar "'"
a g( a) b g a( g) \bar "||"
g g g( a) \bar "'"
 a a a a g a \bar "" f g a \bar "'"
g( a) b( g) a( g) \bar "||"
}


agnustune = \relative c'' {
  \cadenzaOn
  \stemOff
        \override Staff.TimeSignature.stencil = ##f
g g g( a) a \bar "'"
 a a a \bar "" g a f g a \bar "'"
g a b g a( g) g \bar "||"
g g g( a) a \bar "'"
 a a a \bar "" g a f g a \bar "'"
g a b g a( g) g \bar "||"
g g g( a) a \bar "'"
 a a a \bar "" g a f g a \bar "'"
g a b g a( g) g \bar "||"
}



kyrietext = \lyricmode {
Ky -- ri -- e, e -- lé -- i -- son.
Ky -- ri -- e, e -- lé -- i -- son.
Christ -- e, e -- lé -- i -- son.
Christ -- e, e -- lé -- i -- son.
Ky -- ri -- e, e -- lé -- i -- son.
Ky -- ri -- e, e -- lé -- i -- son.
}

lordtext = \lyricmode {
Lord, have mer -- cy.
Lord, have mer -- cy.
Christ, have mer -- cy.
Christ, have mer -- cy.
Lord, have mer -- cy.
Lord, have mer -- cy.
}

lenttext = \lyricmode {
Praise and honour to you, Lord Je -- sus; 
Praise and hon -- our, Lord Je -- sus Christ! 
}

gloriatext = \lyricmode {
Glo -- ry to God in the high -- est
and on earth peace to peo -- ple of good will.
We praise you, we bless you, we a -- dore you, we glo -- ri -- fy you,
we give you thanks, for your great glo -- ry.
Lord God, hea -- ven -- ly King, O God, al -- might -- y Fa -- ther,
Lord Je -- sus Christ, On -- ly Be -- got -- ten Son,
Lord God, Lamb of God, Son of the Fa -- ther,
you take a -- way the sins of the world, 
have mer -- cy on us;
you take a -- way the sins of the world, 
re -- ceive our prayer;
you are seat -- ed at the right hand of the Fa -- ther,
have mer -- cy on us.
For you a -- lone are the Ho -- ly One,
you a -- lone are the Lord,
you a -- lone are the Most High, Je -- sus Christ,
with the Ho -- ly Spi -- rit, in the glo -- ry of God the Fa -- ther.
A -- men.
}

alleluiatext = \lyricmode {
Al -- le -- lu -- ia,
al -- le -- lu -- ia,
al -- le -- lu -- ia.
}

holytext = \lyricmode {
Ho -- ly, Ho -- ly,
Ho -- ly
Lord God of hosts.
Hea -- ven and earth are \time 2/4 full of your \time 4/4 glo -- ry.
Ho -- san -- na in the high -- est.
Bles -- sed is he who comes in the name of the Lord.
Ho -- san -- na in the high -- est.
}

sanctustext = \lyricmode {
San -- ctus,
San -- ctus,
San -- ctus Dó -- mi -- nus De -- us Sá -- ba -- oth.
Ple -- ni sunt cae -- li et ter -- ra gló -- ri -- a tu -- a.
Ho -- san -- na in ex -- cel -- sis.
Be -- ne -- dí -- ctus qui ve -- nit in nó -- mi -- ne Dó -- mi -- ni.
Ho -- san -- na in ex -- cel -- sis.
}

firsttext = \lyricmode {
We pro -- claim your Death, O Lord,
and con -- fess your Re -- sur -- rec -- tion
un -- til you come a -- gain.
}
secondtext = \lyricmode {
When we eat this Bread and drink this Cup,
we pro -- claim your Death, O Lord,
un -- til you come a -- gain.
}
thirdtext = \lyricmode {
Save us, Sa -- viour of the world,
for by your Cross and Re -- sur -- rec -- tion
you have set us free.
}

ourfathertext = \lyricmode {
Our Fa -- ther, who art in heav -- en,
hal -- lowed be thy name;
thy king -- dom come,
thy will be done on earth as it is in heav -- en.
Give us this day our dai -- ly bread,
and for -- give us our tres -- pass -- es,
as we for -- give those who tres -- pass a -- gainst us;
and lead us not in -- to temp -- ta -- tion,
but de -- liv -- er us from e -- vil.
}

lambtext = \lyricmode {
Lamb of God, you take a -- way the sins of the world, have mer -- cy on us,
Lamb of God, you take a -- way the sins of the world, have mer -- cy on us,
Lamb of God, you take a -- way the sins of the world, grant us peace.
}

agnustext = \lyricmode {
A -- gnus De -- i, qui tol -- lis pec -- cá -- ta mun -- di:
mi -- se -- ré -- re no -- bis.
A -- gnus De -- i, qui tol -- lis pec -- cá -- ta mun -- di:
mi -- se -- ré -- re no -- bis.
A -- gnus De -- i, qui tol -- lis pec -- cá -- ta mun -- di:
do -- na no -- bis pa -- cem.
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \kyrietune
    }
    \new Lyrics \lyricsto "one" \kyrietext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \lordtune
    }
    \new Lyrics \lyricsto "one" \lordtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \gloriatune
    }
    \new Lyrics \lyricsto "one" \gloriatext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \alleluiatune

}
    \new Lyrics \lyricsto "one" \alleluiatext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \holytune
    }
    \new Lyrics \lyricsto "one" \holytext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \sanctustune
    }
    \new Lyrics \lyricsto "one" \sanctustext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \weproclaimtune
    }
    \new Lyrics \lyricsto "one" \firsttext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \whenwetune
    }
    \new Lyrics \lyricsto "one" \secondtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \saveustune
    }
    \new Lyrics \lyricsto "one" \thirdtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \ourfathertune-au
    }
    \new Lyrics \lyricsto "one" \ourfathertext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
  }
  \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \lambtune
    }
    \new Lyrics \lyricsto "one" \lambtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \agnustune
    }
    \new Lyrics \lyricsto "one" \agnustext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
  }
  \midi { }
}
}

