\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  system-system-spacing = #'((basic-distance . 2) (padding . 4))
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}


#(define (override-color-for-all-grobs color)
  (lambda (context)
   (let loop ((x all-grob-descriptions))
    (if (not (null? x))
     (let ((grob-name (caar x)))
      (ly:context-pushpop-property context grob-name 'color color)
      (loop (cdr x)))))))


lordtune = \relative c' {
\key d \major
fis4. g8 a4( fis8 a) d,2 r2
e8 d e d a' fis4.
e8 d e d a'4( fis8 a) d,2. r4
}

lordtext = \lyricmode {
Lord, have mer -- cy.
Ky -- ri -- e, e -- léi -- son.
Ky -- ri -- e, e -- léi -- son.
}
christtext = \lyricmode {
Christ, have mer -- cy.
Christ _ -- e, e -- léi -- son.
Christ _ -- e, e -- léi -- son.
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \lordtune 

}
    \new Lyrics \lyricsto "one" \lordtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \lordtune 

}
    \new Lyrics \lyricsto "one" \christtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


