\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  system-system-spacing = #'((basic-distance . 2) (padding . 4))
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}


#(define (override-color-for-all-grobs color)
  (lambda (context)
   (let loop ((x all-grob-descriptions))
    (if (not (null? x))
     (let ((grob-name (caar x)))
      (ly:context-pushpop-property context grob-name 'color color)
      (loop (cdr x)))))))

kyrietune = \relative c' {
    \key d \major
    d4. e8 fis4 fis | g4( fis8 e) fis2 |
    d4. e8 fis4 fis | g4( fis8 e) fis2 |
    fis4. g8 a4 a | b2 a |
    fis4. g8 a4 a | b2 a |
    a4. g8 fis4 e8( d) e4( d) d2 |
    a'4. g8 fis4 e8( d) e4( d) d2 |
}

gloriatune = \relative c' {
    \key d \major
d4 d8 d e4 fis8 g | a4 a2 a8 a | b2 a4. g8 | fis4 e8 d e2 |
d2. a'4 | b2 a4 fis | e2 fis4 a8 a | b2 a4 fis |
\tuplet 3/2 {e4 d4 e} fis a | b b a a | b b a a |
d,2 a' | \tuplet 3/2 {b4 a g} a4 a | d, d g fis | e2 2 |
d2 e4 e | a2. r4 | \tuplet 3/2 {d,4 d d} e e | a2. r4 |
b2 a4( fis) | e e fis2 | d2 g4 fis | e2 e |
d4 d8 d e4 e | \tuplet 3/2 {a4 a a} fis4 d | g2 fis4 fis | e1 |
d4 d8 d e4 e | \tuplet 3/2 {a4 a a} fis4 d | g2 fis | e1 |
d4 d e4 e | \tuplet 3/2 {a4 a a} c4 b8 b | a4 a2 d,4 |
g2 fis4 fis | e2. a4 | b4. b8 a4 fis8 fis | e4 e fis2 |
b4 b a2 | b4 b a2 | d,4 d e e8 e |
a4 a c b | a2. a8 a | b4 b a8 a a a |
\tuplet 3/2 {d,4 d d} a'4 a | b2 a \bar "||" d2 a | b( a) a1 \bar "|."
}

sanctustune = \relative c' {
\key f \major
f4 f g g | a2 a | bes a4 a | g1 |
a4 a bes bes | c2 f,4 bes | a2( g) | f2. c4 |
f4 g a bes | a2( g) | f2. r4 \bar "||"
\tuplet 3/2 {a4 a a} g4 g | f2 e4 e | d2 f4 f |
g2. c,4 | f g a bes | a2( g) | f1 \bar "||"
}

acclaone = \relative c' {
\key f \major
\partial 2
c4 c | f f g g | a2 a4 a | bes bes a g |
c2 c4 c | bes a a g a1 \bar "|."
}

acclatwo = \relative c' {
\key f \major
\partial 2
c4 c | f2 g | a2. a4 | bes2 a4( g) | a2 c,4 c |
f4 g a bes | c2. c4 | bes a a g | a1 \bar "|."
}

acclathree = \relative c' {
\key f \major
\partial 2
c4 c | f4 f g g a8 r8 a4 a a |
bes4 bes a g | c2 c | bes4 a a g | a1 \bar "|."
}

amentune = \relative c' {
\key bes \major
f2( g) | a1 | bes2( a4 g) | c1 | bes2( a4 g) a1 \bar "|."
}

lambtune = \relative c' {
\key bes \major
\partial 4
\time 2/4
d8 ees | f4. f8 | bes8 bes bes a |
g4 f8 ees | f4. f8 | f4 d8 c d2 \bar "||"
r4 d8 ees | f4. f8 | g8 g g g |
a4 a8 a | bes4. bes8 | g4 a8 g f2 \bar "||"
r4 d8 ees | f4. f8 | bes8 bes bes a |
g4 f8 ees | f2 | g4 bes | bes2 \bar "|."
}

kyrietext = \lyricmode {
Ky -- ri -- e, e -- léi -- son.
Ky -- ri -- e, e -- léi -- son.
Christ _ -- e, e -- léi -- son.
Christ _ -- e, e -- léi -- son.
Ky -- ri -- e, e -- léi -- son.
Ky -- ri -- e, e -- léi -- son.
}

lordtext = \lyricmode {
Lord, _ _ have mer -- cy.
Lord, _ _ have mer -- cy.
Christ, _ _ have mer -- cy.
Christ, _ _ have mer -- cy.
Lord, _ _ have mer -- cy.
Lord, _ _ have mer -- cy.
}

gloriatext = \lyricmode {
Glo -- ry to God in the high -- est
and on earth peace to peo -- ple of good will.
We praise you, we bless you, we a -- dore you, we glo -- ri -- fy you,
we give you thanks, for your great glo -- ry.
Lord God, hea -- ven -- ly King, O God, al -- might -- y Fa -- ther,
Lord Je -- sus Christ, On -- ly Be -- got -- ten Son,
Lord God, Lamb of God, Son of the Fa -- ther,
you take a -- way the sins of the world, 
have mer -- cy on us;
you take a -- way the sins of the world, 
re -- ceive our prayer;
you are seat -- ed at the right hand of the Fa -- ther,
have mer -- cy on us.
For you a -- lone are the Ho -- ly One,
you a -- lone are the Lord,
you a -- lone are the Most High, Je -- sus Christ,
with the Ho -- ly Spi -- rit, in the glo -- ry of God the Fa -- ther.
A -- men,
A -- men.
}

holytext = \lyricmode {
Ho -- ly, Ho -- ly,
Ho -- ly
Lord God of hosts.
Heav'n and earth are full of your glo -- ry.
Ho -- san -- na in the high -- est.
Bles -- sed is he who comes in the name of the Lord.
Ho -- san -- na in the high -- est.
}



firsttext = \lyricmode {
We pro -- claim your Death, O Lord,
and con -- fess your Re -- sur -- rec -- tion
un -- til you come a -- gain.
}

secondtext = \lyricmode {
When we eat this Bread and drink this Cup,
we pro -- claim your Death, O Lord,
un -- til you come a -- gain.
}

thirdtext = \lyricmode {
Save us, Sa -- viour of the world,
for by your Cross and Re -- sur -- rec -- tion
you have set us free.
}

amentext = \lyricmode {
A -- men, A -- men, A -- men.
}

lambtext = \lyricmode {
Lamb of God, you take a -- way the sins of the world, have mer -- cy on us,
Lamb of God, you take a -- way the sins of the world, have mer -- cy on us,
Lamb of God, you take a -- way the sins of the world, grant us peace.
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
 -\tweak layer #-1
 -\markup {
   \with-dimensions #'(0 . 0) #'(0 . 0)
   % specify color
   \with-color "#140578"
   % specify size
   \filled-box #'(-1000 . 1000) #'(-1000 . 4000) #0
 }          \kyrietune 

}
    \new Lyrics \lyricsto "one" \kyrietext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
 -\tweak layer #-1
 -\markup {
   \with-dimensions #'(0 . 0) #'(0 . 0)
   % specify color
   \with-color "#140578"
   % specify size
   \filled-box #'(-1000 . 1000) #'(-1000 . 4000) #0
 }          \kyrietune 

}
    \new Lyrics \lyricsto "one" \lordtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
 -\tweak layer #-1
 -\markup {
   \with-dimensions #'(0 . 0) #'(0 . 0)
   % specify color
   \with-color "#140578"
   % specify size
   \filled-box #'(-1000 . 1000) #'(-1000 . 4000) #0
 }          \gloriatune 

}
    \new Lyrics \lyricsto "one" \gloriatext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
 -\tweak layer #-1
 -\markup {
   \with-dimensions #'(0 . 0) #'(0 . 0)
   % specify color
   \with-color "#140578"
   % specify size
   \filled-box #'(-1000 . 1000) #'(-1000 . 4000) #0
 }          \sanctustune 

}
    \new Lyrics \lyricsto "one" \holytext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
 -\tweak layer #-1
 -\markup {
   \with-dimensions #'(0 . 0) #'(0 . 0)
   % specify color
   \with-color "#140578"
   % specify size
   \filled-box #'(-1000 . 1000) #'(-1000 . 4000) #0
 }          \acclaone 

}
    \new Lyrics \lyricsto "one" \firsttext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
 -\tweak layer #-1
 -\markup {
   \with-dimensions #'(0 . 0) #'(0 . 0)
   % specify color
   \with-color "#140578"
   % specify size
   \filled-box #'(-1000 . 1000) #'(-1000 . 4000) #0
 }          \acclatwo 

}
    \new Lyrics \lyricsto "one" \secondtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
 -\tweak layer #-1
 -\markup {
   \with-dimensions #'(0 . 0) #'(0 . 0)
   % specify color
   \with-color "#140578"
   % specify size
   \filled-box #'(-1000 . 1000) #'(-1000 . 4000) #0
 }          \acclathree

}
    \new Lyrics \lyricsto "one" \thirdtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
 -\tweak layer #-1
 -\markup {
   \with-dimensions #'(0 . 0) #'(0 . 0)
   % specify color
   \with-color "#140578"
   % specify size
   \filled-box #'(-1000 . 1000) #'(-1000 . 4000) #0
 }          \amentune

}
    \new Lyrics \lyricsto "one" \amentext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
 -\tweak layer #-1
 -\markup {
   \with-dimensions #'(0 . 0) #'(0 . 0)
   % specify color
   \with-color "#140578"
   % specify size
   \filled-box #'(-1000 . 1000) #'(-1000 . 4000) #0
 }          \lambtune

}
    \new Lyrics \lyricsto "one" \lambtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



