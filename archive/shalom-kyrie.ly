\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

  a'4 a8( g) a4 a
  a4 a8( g) a4 a
  g e8( d) e4 e
  g e8( d) e4 e
\time 6/4
  c'4 b8( a) g4( a) a2
  c4 b8( a) g4( a) a2
}

text = \lyricmode {
Lord have mer -- cy
Lord have mer -- cy
  Christ have mer -- cy
Christ have mer -- cy
 Lord have mer -- cy
  Lord have mer -- cy
}

\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \melody
    }
    \new Lyrics \lyricsto "one" \text
  >>
  \layout {  
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
        \override LyricText #'font-size = #1
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
