\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

g'2 g4 e8( d) c2 e4 f g2( a4 b) c2 \break \bar "|"
 g4 g a2 a4 a g f e d c2. \bar "|"

}

lastmelody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

g'2 g4 e8( d) c2 e4 f g2( a4 b) c2 \break \bar "|"
 g4 g a2 a4 a g f e d c1
f2( g a c2) c1 \bar "||"

}

firstverse = \relative c' {
\clef treble
\key c \major
\time 4/4
\partial 4
g'4 g2 a4 g g2 a4 a8 a c2 b4 a a8 g fis4 g 
e e2 e e2. e4 a2 g4( d) e4 e2
g4 a2 a8 g a4 g2. g4 c2. c4 b4 a g( fis) g2 r2
}

secondverse = \relative c' {
\clef treble
\key c \major
\time 4/4
e2 c4 d e1
a4 a8 a g4 e8( d) e2. r4
c'4( b) a2
c4 b a2
c4( b) a g e e2 \break \bar "|"
g4 a4. a8 g4 g a4 a8 a g4 \bar "|" e c2 d4 d e2. \break \bar "|"
g4 a4. a8 g4 g a4 a8 a g4 \break \bar "|" e c'2 b4( a) gis1 \break \bar "|"
a4 a g8 g g g a4 a e d e e2 e4 a2 a4 a gis2 r2
}

thirdverse = \relative c' {
\clef treble
\key c \major
\time 4/4
\partial 4
e4 c'2 b4( a) b2 e,4 e a a b2 r1
a2 a e c4 d e2. r4
c'2 b4( a) b2 e,4 e a2 a e4 d e2
a4 a g g a a e e f2 e4 d \time 6/4 e f g2 g
}

text = \lyricmode {
Glo -- ry to God in the high -- est
and on earth peace to peo -- ple of good will.
}
lasttext = \lyricmode {
Glo -- ry to God in the high -- est
and on earth peace to peo -- ple of good will.
A -- men.
}
firsttext = \lyricmode {
We praise you, we bless you, we a -- dore you, we glo -- ri -- fy you,
we give you thanks, for your great glo -- ry.
Lord God, hea -- ven -- ly King, O God al -- might -- y Fa -- ther,
}
secondtext = \lyricmode {
Lord Je -- sus Christ, On -- ly Be -- got -- ten Son,
Lord God, Lamb of God, Son of the Fa -- ther,
you take a -- way the sins of the world, 
have mer -- cy on us;
you take a -- way the sins of the world, 
re -- ceive our prayer;
you are seat -- ed at the right hand of the Fa -- ther,
have mer -- cy on us.
}
thirdtext = \lyricmode {
For you a -- lone are the Ho -- ly One,
you a -- lone are the Lord,
you a -- lone are the Most High, Je -- sus Christ,
with the Ho -- ly Spi -- rit, in the glo -- ry of God the Fa -- ther.
}

\bookpart {
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \melody
    }
    \new Lyrics \lyricsto "one" \text
  >>
  \layout {  
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
        \override LyricText #'font-size = #1
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \firstverse
    }
    \new Lyrics \lyricsto "one" \firsttext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
        \override LyricText #'font-size = #1
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \secondverse
    }
    \new Lyrics \lyricsto "one" \secondtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
        \override LyricText #'font-size = #1
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}


\bookpart {
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \thirdverse
    }
    \new Lyrics \lyricsto "one" \thirdtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
        \override LyricText #'font-size = #1
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \lastmelody
    }
    \new Lyrics \lyricsto "one" \lasttext
  >>
  \layout {  
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
        \override LyricText #'font-size = #1
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}


