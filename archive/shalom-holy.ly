\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

c4 c e e g2 g
a4( g8 f) g4 c, d1
e4 e8 d c4 c e f8 g a2 g4 g c4 b8( a) g4 f8( e) d2 c
}

benelody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

g'4 g a a g g8 g e4 f8 f g2. g4 c b8( a) g4 f8( e) d2 c

}

text = \lyricmode {
Ho -- ly
Ho -- ly
Ho -- ly
Lord God of hosts.
Hea -- ven and earth are \time 2/4 full of your \time 4/4 glo -- ry.
Ho -- san -- na in the high -- est.
}
benedictus = \lyricmode {
Bless'd is he who comes in the name of the Lord.
Ho -- san -- na in the high -- est.
}

\bookpart{
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \melody
    }
    \new Lyrics \lyricsto "one" \text
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart{
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \benelody
    }
    \new Lyrics \lyricsto "one" \benedictus
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}
