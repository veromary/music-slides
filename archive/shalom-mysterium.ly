\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

g'2 g4 e8( d) c2 e4 f g2( a4 b) c2 \break \bar "|"
 g4 g a2 a4 a g f e d c2. \bar "|"

}

lastmelody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

g'2 g4 e8( d) c2 e4 f g2( a4 b) c2 \break \bar "|"
 g4 g a2 a4 a g f e d c1
f2( g a c2) c1 \bar "||"

}

firstverse = \relative c' {
\clef treble
\key c \major
\time 4/4
\partial 8
c16 d e8 e d d c4. 
c16 d e8 g g b a a4.
r8 a c c c4 b c1 \bar "||"
}

secondverse = \relative c' {
\clef treble
\key c \major
\time 2/4
e8 d e g f e f a g2
a8 a b c \time 3/4 c g g4. g8 
\time 2/4 a8 c c b c2 \bar "||"
}

thirdverse = \relative c'' {
\clef treble
\key c \major
\time 2/4
g4 g a8 g f a g2
a4 b8 a g4 e gis a8( b) b4 a a b8( c) c4 b c2 \bar "||"
}

text = \lyricmode {
}
lasttext = \lyricmode {
Glo -- ry to God in the high -- est
and on earth peace to peo -- ple of good will.
A -- men.
}
firsttext = \lyricmode {
We pro -- claim your Death, O Lord,
and pro -- fess your Res -- ur -- rec -- tion
un -- til you come a -- gain.
}
secondtext = \lyricmode {
When we eat this Bread and drink this Cup,
we pro -- claim your Death, O Lord,
un -- til you come a -- gain.
}
thirdtext = \lyricmode {
Save us, Sa -- viour of the world,
for by your Cross and Res -- ur -- rec -- tion
you have set us free.
}

\bookpart {
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \firstverse
    }
    \new Lyrics \lyricsto "one" \firsttext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
        \override LyricText #'font-size = #1
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi {
    \tempo 4 = 80
  }
}
}

\bookpart {
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \secondverse
    }
    \new Lyrics \lyricsto "one" \secondtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
        \override LyricText #'font-size = #1
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi {
  \tempo 4 = 80
}
}
}


\bookpart {
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \thirdverse
    }
    \new Lyrics \lyricsto "one" \thirdtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
        \override LyricText #'font-size = #1
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi  {
  \tempo 4 = 80
}
}
}


