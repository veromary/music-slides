\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}

melody = \relative c'' {
  \clef treble
  \key c \major
  \time 4/4
\partial 4
g8 g a4. a8 c c b a g4 g8 f e4. e8 e4 e8 d e4 \break \bar "|"
c8 c d4. d8 f f e f g4 e8 f g4. g8 a4 a8 g a4 \break \bar "|"
}

benelody = \relative c'' {
  \clef treble
  \key c \major
  \time 4/4
\partial 4
c8 c c4. c8 b b a a g4 a8 a b2 c4 b a \bar "||"
}

text = \lyricmode {
Lamb of God, you take a -- way the sins of the world, have mer -- cy on us,
Lamb of God, you take a -- way the sins of the world, have mer -- cy on us,
}
benedictus = \lyricmode {
Lamb of God, you take a -- way the sins of the world, grant us peace.
}

\bookpart{
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \melody
    }
    \new Lyrics \lyricsto "one" \text
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart{
\score{
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \benelody
    }
    \new Lyrics \lyricsto "one" \benedictus
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}
