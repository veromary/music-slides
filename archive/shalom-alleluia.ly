\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 3/4

  g'2 g4 e4.( f8) g4
  a4.( g8) f4
  g2 g4
  c4.( b8) a4 g2 e4
  f4 e d c2.
}

text = \lyricmode {
Al -- le -- lu -- ia, Al -- le -- lu -- ia,
Al -- le -- lu -- ia, Al -- le -- lu -- ia.
}

lenttext = \lyricmode {
Praise and honour to you, Lord Je -- sus; 
Praise and hon -- our, Lord Je -- sus Christ! 
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \melody
    }
    \new Lyrics \lyricsto "one" \text
  >>
  \layout {  
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
      \melody
    }
    \new Lyrics \lyricsto "one" \lenttext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
}
