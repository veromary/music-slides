\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

c2( d e4. f8) g2 a( c) c1 \bar "||"
}

text = \lyricmode {
A -- men, A -- men.
}

\score{
  <<
    \new Voice = "one" 
    {
      \melody
    }
    \new Lyrics \lyricsto "one" \text
  >>
  \layout {  
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #3
    }
    \context {
      \Score
        \omit BarNumber
    }
  }
  \midi { }
}
