# music-slides

A local parish has slides for Mass.

Here are the metrics they follow:

Background: #140678
Text: Tahoma Size 44 #fffe01
Slide size: 4:3

* ICEL chant links: [ICEL web](https://www.icelweb.org/musicfolder/openmusic.php)
* Corpus Christi Watershed has [ICEL links](https://www.ccwatershed.org/icel/) and also [new free Mass settings](https://www.ccwatershed.org/Mass/)

### TODO

new slide format: HD 16:9

High Definition (HD)	720p	16:9	1280 x 720

So, it was making PDFs with lilypond, then doctoring them with Inkscape, then maybe some GIMP work.

This time I'll remember to take notes!


### Masses to include

- [x] Mass Shalom
- [x] Mass of St Francis
- [x] Mass of Glory and Praise
- [ ] Mass of Creation
- [ ] Mass Porta Fidei
- [ ] Mass Te Deum
- [x] Missa Primativa
- [ ] Mass Jubilee
- [x] Mass For Moderns
- [ ] Mass For a Pilgrim People

### Technical

[this snippet repo](https://lsr.di.unimi.it/LSR/Search?q=color) describes a few different ways with colours

I have set the lilypond page to a7 landscape. I import each page into Inkscape using the Cairo/Poppler thing, then export as png at 400 dpi. Then that png goes into a pptx using an online Excel thing.

It feels like there should be a more straightforward way, but the parish in question uses Excel for their Mass slides, so they want something easy to paste each week.

It would be great to do the chant Mass settings in a similar way, but with square notes.

### New Workflow 16/9/24:

* gs -sDEVICE=pngalpha -sOutputFile=filename-%02d.png -r400
* for i in filestar.png; do convert $i -trim ${i/name/trim}; done

so don't have to fiddle with Inkscape any more. But still pasting each image into the pptx.

