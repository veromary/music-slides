\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  system-system-spacing = #'((basic-distance . 2) (padding . 4))
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}


#(define (override-color-for-all-grobs color)
  (lambda (context)
   (let loop ((x all-grob-descriptions))
    (if (not (null? x))
     (let ((grob-name (caar x)))
      (ly:context-pushpop-property context grob-name 'color color)
      (loop (cdr x)))))))


gloriatune = \relative c' {
\key d \major
fis8 fis fis a~ a a4 a8 b2 a4 fis8 e d4. d8~ d d fis fis e( fis) e d~ d2
}

gloriaone = \relative c' {
\key d \major
\partial 4
fis4 fis4. fis8~ fis4 fis e8( fis g) fis8~ fis4 \break \bar "|" fis8 a b4. a8~ a4 g
g8 fis e fis~ fis8 \break \bar "|" d8 fis a g4. b8 a4 e fis fis2 r4
b4.( fis8) fis4( g) a8 a g fis~ fis4 e8( d) d4. d8 \tuplet 3/2 { g4( a) b } b8( a) a4 r2
}

gloriatwo = \relative c' {
\key d \major
\partial 4
fis4 fis fis8 fis~ fis fis fis fis e( fis) g fis~ fis2 b4 b a g 
a2 fis4 e8 d e4 e2 fis4 fis fis8 fis~ fis4 fis e8 fis g fis~ fis4 e
d2 d4 e fis2 r4 fis4 fis fis8 fis~ fis4 fis e8 fis g fis~ fis4 d4
b'2 a a r2 a4 a8 a~ a a g fis g4. g8~ g a g4 
fis2~( fis8 g fis4) e2 r4 d d'2 cis4 b a2 r2
}

gloriathree = \relative c' {
\key d \major
\partial 4
fis4 fis fis8 fis~ fis4 fis8 fis e( fis) g fis~ fis2 b4 b a8 a g4 fis2 r2
fis4 fis8 fis~ fis2 e8 fis g fis~ fis2 b2 a a r2
a4 a8 a~( a g) fis4 g4. g8~ g8 a g4 fis2~ fis8 g fis4
e2 r4 d d'2( cis4 b) a2 r2
}

glorialast = \relative c' {
\key d \major
fis8 fis fis a~ a a4 a8 b2 a4 fis8 e d4. d8~ d d fis fis e( fis) e d~ d2
\time 3/4 g4( fis e) 
\time 4/4 fis1 \bar "|."
}

gloriatext = \lyricmode {
Glo -- ry to God in the high -- est
and on earth peace to peo -- ple of good will.
A -- men.
}

gloriafirst = \lyricmode {
We praise you, we bless you, we a -- dore you, we glo -- ri -- fy you,
we give you thanks, for your great glo -- ry.
Lord God, hea -- ven -- ly King, O God, al -- might -- y Fa -- ther,
}
gloriasecond = \lyricmode {
Lord Je -- sus Christ, On -- ly Be -- got -- ten Son,
Lord God, Lamb of God, Son of the Fa -- ther,
you take a -- way the sins of the world, 
have mer -- cy on us;
you take a -- way the sins of the world, 
re -- ceive our prayer;
you are seat -- ed at the right hand of the Fa -- ther,
have mer -- cy on us.
}
gloriathird = \lyricmode {
For you a -- lone are the Ho -- ly One,
you a -- lone are the Lord,
you a -- lone are the Most High, Je -- sus Christ,
with the Ho -- ly Spi -- rit, in the glo -- ry of God the Fa -- ther.
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \gloriatune 

}
    \new Lyrics \lyricsto "one" \gloriatext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \gloriaone 

}
    \new Lyrics \lyricsto "one" \gloriafirst
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \gloriatwo

}
    \new Lyrics \lyricsto "one" \gloriasecond
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \gloriathree

}
    \new Lyrics \lyricsto "one" \gloriathird
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \glorialast

}
    \new Lyrics \lyricsto "one" \gloriatext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


alleluiatune = \relative c'' {
\key d \major
\time 3/4
a2 d4 d4.( cis8 b4) a2. r2. fis2. b2. a4.( g8 fis4) e2.
a2 d4 d4.( cis8 b4) a2. r2. g2 fis4 e4.( d8 cis4) d2.~ d2.
}

alleluiatext = \lyricmode {
Al -- le -- lu -- ia, Al -- le -- lu -- ia,
Al -- le -- lu -- ia, Al -- le -- lu -- ia.
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \alleluiatune

}
    \new Lyrics \lyricsto "one" \alleluiatext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}

lenttune = \relative c'' {
\time 2/4
c4 c b8 a g4 a g c, d e2 r2
c'4 c b8 a g4 a g c, d c2
}

lenttext = \lyricmode {
Praise and hon -- our to you, Lord Je -- sus Christ!
Praise and hon -- our to you, Lord Je -- sus Christ! 
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \lenttune

}
    \new Lyrics \lyricsto "one" \lenttext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


holytune = \relative c' {
\key g \major
\time 4/4
d4.( e8) d2 g4.( d8) d2 e4 fis8( g) d8. d16~ d8 c d2. r4
g4 g8 g g4 b e, e8 e e8. a16~ a8 \break \bar "|" g fis4 e8( d) c'4 b8( a)
b8( a) g2 r4 e4. e8 fis4. fis8 fis4 g8 a c8. b16~ b8 a
g2 r2 r2 r4. g8 fis4 e8( d) c'4 b8( a) b( a) g2.
}

holytext = \lyricmode {
Ho -- ly, Ho -- ly,
Ho -- ly
Lord God of hosts.
Hea -- ven and earth are full of your glo -- ry.
Ho -- san -- na in the high -- est.
Bless'd is he who comes in the name of the Lord.
Ho -- san -- na in the high -- est.
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \holytune 

}
    \new Lyrics \lyricsto "one" \holytext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


firsttext = \lyricmode {
We pro -- claim your Death, O Lord,
and con -- fess your Re -- sur -- rec -- tion
un -- til you come a -- gain.
}

secondtext = \lyricmode {
When we eat this Bread and drink this Cup,
we pro -- claim your Death, O Lord,
un -- til you come a -- gain.
}

thirdtext = \lyricmode {
Save us, Sa -- viour of the world,
for by your Cross and Re -- sur -- rec -- tion
you have set us free.
}

amentext = \lyricmode {
A -- men. A -- men. A -- men.
A -- men. A -- men. A -- men.
A -- men. A -- men. A -- men.
}

acclaone = \relative c' {
\key g \major
d4 d d d e d8( c) d2 g4 g fis g 
c b8( a) a4 g4~ g2 r4 a4 g fis e fis g1
}

acclatwo = \relative c' {
\key g \major
d8 d d e d4 d d g \break \bar "|" a2
b8 b b c b4 a8( fis) g2. r4
r8 c b a g4 fis g1
}

acclathree = \relative c' {
\key g \major
d4 d d8 d e fis fis( g4.~ g4) r4
g4 g8 g fis fis g a a4 b2 r4 c4 b8 a g4 fis g1
}

amentune = \relative c' {
\key g \major
b4( c) d2 e4( fis) g2 g fis4 e d1
b4( c) d2 e4( fis) g2 g4( c8 b) a4 g8( fis) g1
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \acclaone 

}
    \new Lyrics \lyricsto "one" \firsttext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \acclatwo 

}
    \new Lyrics \lyricsto "one" \secondtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \acclathree

}
    \new Lyrics \lyricsto "one" \thirdtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \amentune

}
    \new Lyrics \lyricsto "one" \amentext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}





lambtune = \relative c' {
\key f \major
\time 4/4
\partial 4
f8 g a2. a8 g g( f~ f2) r8 f8 e f d d d4 c8 bes
c4( g' f) r8 f8 g f4 f8 f4 d8 c g'( a4.) r4 \break \bar "|" a8 bes
c2. c8 d a16( g f8~ f2) r8 f8 bes a g f g4 a8 bes
c4( a8 g f4) r8 f8 g f4 f8 f4 d8 c g'( a~ a4) r4 \break \bar "|"
f8 g a2. a8 g g( f8~ f2) r8 f8 e f d d d4 c8 bes
c4( g' f) r4 g4.( a8) bes4( c) c2. e,4 f1
}

lambtext = \lyricmode {
Lamb of God, Lamb of God, you take a -- way the sins of the world, have mer -- cy on us, Lamb of God.
Lamb of God, Lamb of God, you take a -- way the sins of the world, have mer -- cy on us, Lamb of God. 
Lamb of God, Lamb of God, you take a -- way the sins of the world, grant us, grant us peace.
}




\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \lambtune

}
    \new Lyrics \lyricsto "one" \lambtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}




