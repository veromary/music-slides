\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  system-system-spacing = #'((basic-distance . 2) (padding . 4))
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}


#(define (override-color-for-all-grobs color)
  (lambda (context)
   (let loop ((x all-grob-descriptions))
    (if (not (null? x))
     (let ((grob-name (caar x)))
      (ly:context-pushpop-property context grob-name 'color color)
      (loop (cdr x)))))))

lordtune = \relative c'' {
    \key g \major
\repeat volta 2 { g8( e g a b4) b | a8( fis4.) e2 } \break
\repeat volta 2 { b'2 g2 a8( fis4.) e2 } \break
\repeat volta 2 { g8( e g a b4) b | a8( fis4.) e2 }
}

kyrietune = \relative c'' {
    \key g \major
\repeat volta 2 { g8( e g) a b4 b | a8 fis4. e2 } \break
\repeat volta 2 { b'2 g4 g a8 fis4. e2 } \break
\repeat volta 2 { g8( e g) a b4 b | a8 fis4. e2 }
}

lordtext = \lyricmode {
Lord, have mer -- cy.
Christ, have mer -- cy.
Lord, have mer -- cy.
}

kyrietext = \lyricmode {
Ky -- ri -- e, e -- lé -- i -- son.
Christ -- e, e -- lé -- i -- son.
Ky -- ri -- e, e -- lé -- i -- son.
}




\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \lordtune 

}
    \new Lyrics \lyricsto "one" \lordtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \kyrietune 

}
    \new Lyrics \lyricsto "one" \kyrietext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}

gloriatune = \relative c' {
\key g \major
\time 3/4
b4 e g fis g a b2. b4 b b b, e g fis g a 
b2. b2 r4 c4( b) a a2. b2 g4 b2 r4 c b a a2 a4 
b4 b g b2 r4 b,4 e g fis2 g8( fis) e4 e d e2 r4 g2.~ g2( c4)
a2.~ a2 r4 g2. g2 c4 a2.~ a4. r8 a8( b)
c4( b) a g( e) d c2. c2 r4 a'4 b c a2.~ a2) r4
b2 b4 a fis a b2.~ b2 r4
c2( a4) b2( g4) a2 b8( c) b2 r4 d4( c b) a( g) fis e2. e2 r4
a4 b c b2 b8( c) d4 c b a2 g8( fis) e4 e d e2.~ 
e4. r8 e4 b'2 b4 g2 a8( b) c2 b8 a g2 e8( fis) g2.( fis2) g8( fis) e2.~ 
e4. r8 e e b'4 b b8 b g4 g a8 b c2( b8 a) g2 e8( fis) g2. fis2 g8( fis)
e2.~ e4. r8 e4 b( e) g fis g a b2 b4 b2 r4 b,( e) g fis g a 
b2.~ b2 r4 c( b) a a2 a8 a b2( g4) b2 r4 b,( e g fis2) g8( fis)
e2.~ e4. r8 e e g2.~ g2 c4 a2. a4. r8 a b c4 b a
g( e) d c2. c2 r4 b( e g fis2 g8 fis) e2.~ e2.
}

gloriatext = \lyricmode {
Glo -- ry to God in the high -- est
and on earth peace to peo -- ple of good will.
We praise you, we bless you, we a -- dore you, we glo -- ri -- fy you,
we give you thanks, for your great glo -- ry.
Lord God, hea -- ven -- ly King, O God, al -- might -- y Fa -- ther,
Lord Je -- sus Christ, On -- ly Be -- got -- ten Son,
Lord God, Lamb of God, Son of the Fa -- ther,
you take a -- way the sins of the world, 
have mer -- cy on us;
you take a -- way the sins of the world, 
re -- ceive our prayer;
you are seat -- ed at the right hand of the Fa -- ther,
have mer -- cy on us.
For you a -- lone are the Ho -- ly One,
you a -- lone are the Lord,
you a -- lone are the Most High, Je -- sus Christ,
with the Ho -- ly Spi -- rit, in the glo -- ry of God the Fa -- ther.
A -- men,
A -- men.
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \gloriatune 

}
    \new Lyrics \lyricsto "one" \gloriatext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}

alleluiatune = \relative c'' {
\key g \major
b2 g a8( fis4.) e2
g2 e fis8( d4.) e2
b'2 g a8( fis4.) e2
}

alleluiatext = \lyricmode {
Al -- le -- lu -- ia, Al -- le -- lu -- ia,
Al -- le -- lu -- ia.
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \alleluiatune 

}
    \new Lyrics \lyricsto "one" \alleluiatext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}




holytune = \relative c'' {
g2.( e8 d) c1 a'2.( e8 f) g1 f2. e8(d)
e1 f2. e8( c) d1 e2 e8 d4. c2. e8( f)
g2 g4 f e1 e2. r8 \break \bar "" c8 c'2. a4 g e d8( e d4)
c2~ c~ c2. r4 \break \bar "" c'2. a4 g2. e8( g) a2 g4 f
e8 f g4 g2~ g2. r8 c,8 c'2. a4 g e d8( e d4) c1~
c2. r8 c8 c'2. a4 g e d8( e d4) c2~ c~ c2. r4
}

holytext = \lyricmode {
Ho -- ly, Ho -- ly,
Ho -- ly
Lord God of hosts.
Hea -- ven and earth are full of your glo -- ry.
Ho -- san -- na in the high -- est.
Blessed is he who comes in the name of the Lord.
Ho -- san -- na in the high -- est.
Ho -- san -- na in the high -- est.
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \holytune 

}
    \new Lyrics \lyricsto "one" \holytext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



firsttext = \lyricmode {
We pro -- claim your Death, O Lord,
and con -- fess your Re -- sur -- rec -- tion
un -- til you come a -- gain.
}

secondtext = \lyricmode {
When we eat this Bread and drink this Cup,
we pro -- claim your Death, O Lord,
un -- til you come a -- gain.
}

thirdtext = \lyricmode {
Save us, Sa -- viour of the world,
for by your Cross and Re -- sur -- rec -- tion
you have set us free.
}

amentune = \relative c'' {
\key g \major
b2( g a8 fis4.) e2
g2( e fis8 d4.) e2
b'2( g a8 fis4.) e2
}

amentext = \lyricmode {
A -- men, A -- men, A -- men.
}

\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \amentune 

}
    \new Lyrics \lyricsto "one" \amentext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



lambtune = \relative c'' {
\key g \major
g1 fis8 d4. e2 b'8( a) g a b4 b a8( g) fis d e2
g2 g fis8 d4. e2
g1 fis8 d4. e2 b'8( a) g a b4 b a8( g) fis d e2
g2 g fis8 d4. e2
g1 fis8 d4. e2 b'8( a) g a b4 b a8( g) fis d e2
g1 fis8 d4. e2~ e1
}


lambtext = \lyricmode {
Lamb Lamb of God, you take a -- way the sins of the world, have mer -- cy on us,
Lamb Lamb of God, you take a -- way the sins of the world, have mer -- cy on us,
Lamb Lamb of God, you take a -- way the sins of the world, grant grant us peace.
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \lambtune 

}
    \new Lyrics \lyricsto "one" \lambtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


