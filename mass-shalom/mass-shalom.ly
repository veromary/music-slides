\version "2.24.2"

\header {
 tagline = #f
}

\paper {
  system-system-spacing = #'((basic-distance . 2) (padding . 4))
  print-page-number = ##f
  #(set-paper-size "a7landscape")
  #(define fonts
    (make-pango-font-tree "Linux Libertine O"
                          "Tahoma"
                          "DejaVu Sans Mono"
                          (/ staff-height pt 20)))
}


#(define (override-color-for-all-grobs color)
  (lambda (context)
   (let loop ((x all-grob-descriptions))
    (if (not (null? x))
     (let ((grob-name (caar x)))
      (ly:context-pushpop-property context grob-name 'color color)
      (loop (cdr x)))))))


lordtune = \relative c' {
  a'4 a8( g) a4 a
  a4 a8( g) a4 a
  g e8( d) e4 e
  g e8( d) e4 e
\time 6/4
  c'4 b8( a) g4( a) a2
  c4 b8( a) g4( a) a2 \bar "||"
 }

gloriatune = \relative c' {
g'2 g4 e8( d) c2 e4 f g2( a4 b) c2 \break \bar "|"
 g4 g a2 a4 a g f e d c2. \bar "|"
} 

gloriaonetune = \relative c' {
\partial 4
g'4 g2 a4 g g2 a4 a8 a c2 b4 a a8 g fis4 g 
e e2 e e2. e4 a2 g4( d) e4 e2
g4 a2 a8 g a4 g2. g4 c2. c4 b4 a g( fis) g2 r2
}

gloriatwotune = \relative c' {
e2 c4 d e1
a4 a8 a g4 e8( d) e2. r4
c'4( b) a2
c4 b a2
c4( b) a g e e2 \break \bar "|"
g4 a4. a8 g4 g a4 a8 a g4 \bar "|" e c2 d4 d e2. \break \bar "|"
g4 a4. a8 g4 g a4 a8 a g4 \break \bar "|" e c'2 b4( a) gis1 \break \bar "|"
a4 a g8 g g g a4 a e d e e2 e4 a2 a4 a gis2 r2
}

gloriathreetune = \relative c' {
\partial 4
e4 c'2 b4( a) b2 e,4 e a a b2 r1
a2 a e c4 d e2. r4
c'2 b4( a) b2 e,4 e a2 a e4 d e2 \break \bar "|"
a4 a g g a a e e f2 e4 d \time 6/4 e f g2 g
}

gloriaamentune = \relative c' {
f2( g a c2) c1 \bar "||"
}

alleluiatune = \relative c' {
  \time 3/4
  g'2 g4 e4.( f8) g4
  a4.( g8) f4
  g2 g4 \break \bar "|"
  c4.( b8) a4 g2 e4
  f4 e d c2.
}

holytune = \relative c' {
c4 c e e g2 g
a4( g8 f) g4 c, d1
e4 e8 d c4 c \time 2/4 e f8 g \time 4/4 a2 g4 g c4 b8( a) g4 f8( e) d2 c \break \bar "|"
g'4 g a a g g8 g e4 \break \bar "|" f8 f g2. g4 c b8( a) g4 f8( e) d2 c
}


acclaone = \relative c' {
\partial 8
c16 d e8 e d d c4. \break \bar "|"
c16 d e8 g g b a a4. \break \bar "|"
r8 a c c c4 b c1 \bar "||"
}

acclatwo = \relative c' {
\time 2/4
e8 d e g f e f a g2
a8 a b c \time 3/4 c g g4. g8 
\time 2/4 a8 c c b c2 \bar "||"
}

acclathree = \relative c'' {
\time 2/4
g4 g a8 g f a g2
a4 b8 a g4 e gis a8( b) b4 a a b8( c) c4 b c2 \bar "||"
}

amentune = \relative c' {
c2( d e4. f8) g2 a( c) c1 \bar "||"
}

lambtune = \relative c'' {
\partial 4
g8 g a4. a8 c c b a g4 g8 f e4. e8 e4 e8 d e4 \break \bar "|"
c8 c d4. d8 f f e f g4 e8 f g4. g8 a4 a8 g a4 \break \bar "|"
c8 c c4. c8 b b a a g4 a8 a b2 c4 b a \bar "||"
}


lordtext = \lyricmode {
Lord, have mer -- cy.
Lord, have mer -- cy.
Christ, have mer -- cy.
Christ, have mer -- cy.
Lord, have mer -- cy.
Lord, have mer -- cy.
}

gloriatext = \lyricmode {
Glo -- ry to God in the high -- est
and on earth peace to peo -- ple of good will.
}
gloriaone = \lyricmode {
We praise you, we bless you, we a -- dore you, we glo -- ri -- fy you,
we give you thanks, for your great glo -- ry.
Lord God, hea -- ven -- ly King, O God, al -- might -- y Fa -- ther,
}
gloriatwo = \lyricmode {
Lord Je -- sus Christ, On -- ly Be -- got -- ten Son,
Lord God, Lamb of God, Son of the Fa -- ther,
you take a -- way the sins of the world, 
have mer -- cy on us;
you take a -- way the sins of the world, 
re -- ceive our prayer;
you are seat -- ed at the right hand of the Fa -- ther,
have mer -- cy on us.
}
gloriathree = \lyricmode {
For you a -- lone are the Ho -- ly One,
you a -- lone are the Lord,
you a -- lone are the Most High, Je -- sus Christ,
with the Ho -- ly Spi -- rit, in the glo -- ry of God the Fa -- ther.
}
gloriaamen = \lyricmode {
A -- men.
}

alleluiatext = \lyricmode {
Al -- le -- lu -- ia, Al -- le -- lu -- ia,
Al -- le -- lu -- ia, Al -- le -- lu -- ia.
}

lenttext = \lyricmode {
Praise and honour to you, Lord Je -- sus; 
Praise and hon -- our, Lord Je -- sus Christ! 
}


holytext = \lyricmode {
Ho -- ly, Ho -- ly,
Ho -- ly
Lord God of hosts.
Hea -- ven and earth are full of your glo -- ry.
Ho -- san -- na in the high -- est.
Bless'd is he who comes in the name of the Lord.
Ho -- san -- na in the high -- est.
}


firsttext = \lyricmode {
We pro -- claim your Death, O Lord,
and con -- fess your Re -- sur -- rec -- tion
un -- til you come a -- gain.
}

secondtext = \lyricmode {
When we eat this Bread and drink this Cup,
we pro -- claim your Death, O Lord,
un -- til you come a -- gain.
}

thirdtext = \lyricmode {
Save us, Sa -- viour of the world,
for by your Cross and Re -- sur -- rec -- tion
you have set us free.
}

amentext = \lyricmode {
A -- men, A -- men.
}

lambtext = \lyricmode {
Lamb of God, you take a -- way the sins of the world, have mer -- cy on us,
Lamb of God, you take a -- way the sins of the world, have mer -- cy on us,
Lamb of God, you take a -- way the sins of the world, grant us peace.
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
 -\tweak layer #-1
 -\markup {
   \with-dimensions #'(0 . 0) #'(0 . 0)
   % specify color
   \with-color "#140578"
   % specify size
   \filled-box #'(-1000 . 1000) #'(-1000 . 4000) #0
 }          \lordtune 

}
    \new Lyrics \lyricsto "one" \lordtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \gloriatune 

}
    \new Lyrics \lyricsto "one" \gloriatext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \gloriaonetune 

}
    \new Lyrics \lyricsto "one" \gloriaone
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
          \gloriatwotune

}
    \new Lyrics \lyricsto "one" \gloriatwo
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \gloriathreetune

}
    \new Lyrics \lyricsto "one" \gloriathree
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \gloriaamentune

}
    \new Lyrics \lyricsto "one" \gloriaamen
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \alleluiatune

}
    \new Lyrics \lyricsto "one" \alleluiatext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \alleluiatune

}
    \new Lyrics \lyricsto "one" \lenttext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}


\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \holytune 

}
    \new Lyrics \lyricsto "one" \holytext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \acclaone 

}
    \new Lyrics \lyricsto "one" \firsttext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \acclatwo 

}
    \new Lyrics \lyricsto "one" \secondtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \acclathree

}
    \new Lyrics \lyricsto "one" \thirdtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \amentune

}
    \new Lyrics \lyricsto "one" \amentext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



\bookpart {
\score {
  <<
    \new Voice = "one" 
      \with { \magnifyStaff #2/3 } {
         \lambtune

}
    \new Lyrics \lyricsto "one" \lambtext
  >>
  \layout {  
  indent = #0
    \context {
      \Lyrics
        \override LyricText.font-family = #'sans
        \override LyricText.font-size = #2
    }
    \context {
      \Score
  \applyContext #(override-color-for-all-grobs "#FFFF41")
        \omit BarNumber
    }
    }
    \midi { }
}
}



